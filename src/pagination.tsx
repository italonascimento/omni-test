import * as React from 'react';

export let Pagination = function(props: any){

  let pagesArr = [];

  let init: number = props.current - 4 >= 0 ? props.current - 4 : 0;
  let final: number = init + 5 < props.pages ? init + 5 : props.pages;

  for(let i = init; i <= final; i++){
    if(i <= props.pages){
      pagesArr.push(i);
    }
  }

  function setPage(page: number){
    props.onPageChange(page);
  }

  return (
    <nav className="pagination">
      <ul>
        {pagesArr.map((page: number) => {
          return (
            <li key={page} className={props.current == page ? 'current' : ''}>
              <a onClick={(e) => setPage(page)}>{ page+1 }</a>
            </li>
          )
        })}
      </ul>
    </nav>
  )
}
