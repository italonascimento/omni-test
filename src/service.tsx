import axios, { AxiosRequestConfig, AxiosPromise } from 'axios';

export class Service{
  static axios = axios.create({
    baseURL: 'https://08ck87ia8b.execute-api.us-west-2.amazonaws.com/test/',
    headers: {'x-api-key': 'h4kkbFmdgE77TEgA6pWm49CNSxvDsAyy8pe6n4xM'}
  })
}
