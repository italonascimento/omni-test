import * as React from 'react';

export let ItemsPerPage = function(props: any){
  let options = [
    {
      text: '10 items por página',
      quantity: 10
    },
    {
      text: '16 items por página',
      quantity: 16
    },
    {
      text: '25 items por página',
      quantity: 25
    }
  ]

  function changeHandler(e: any){
    props.onSelect(e.target.value);
  }

  return (
    <select className="items-per-page" onChange={changeHandler}>
      {options.map((option) => {
        return (
          <option selected={option.quantity == props.current} key={option.quantity} value={option.quantity}>
            {option.text}
          </option>
        )
      })}
    </select>
  )
}
