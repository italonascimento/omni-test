import * as React from 'react';

export let Loading = function(props: any){
  if(props.show){
    return (
      <span className="loading">Carregando...</span>
    )
  }else{
    return (
      <span></span>
    )
  }
}
