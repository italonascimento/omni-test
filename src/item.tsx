import * as React from 'react';
import { Service } from './service';

export class Item extends React.Component<any, any>{

  private axios: any;
  private line: string = '';

  constructor(props: any){
    super(props);
    // console.log(this.props.data)

    this.state = {line: ''};
    this.axios = Service.axios;
    this.fetchData();
  }

  fetchData(){
    // console.log('fetching: ', 'product/'+this.props.data.products[0].id)

    this.axios.get('product/'+this.props.data.products[0].id).then((res: any) => {
      this.setState({line: res.data.data.line});
    }).catch((err: any) => {
      console.log('err: ', err);
    })
  }

  formatedPrice(price: number){
    if(price){
      let formated: string = price + '';
      formated = formated.replace(/(\d{2}$)/, ',$1');

      return 'R$' + formated;
    }

    return '';
  }

  render(){
    return(
      <li className="item">
        <div className="item-pics">
          <div className="item-pic">
            <img src={'http://dgn7v532p0g5j.cloudfront.net/unsafe/200x200/'+this.props.data.photos.still} />
          </div>
        </div>
        <div className="item-details">
          <h3 className="item-title">
            {this.props.data.title}
          </h3>
          <p className="item-text">
            {this.state.line}
          </p>
          <p className="item-price">
            <span className="max">
              {this.formatedPrice(this.props.data.price.max)}
            </span> {this.props.data.price.max ? ' por ' : ''} <span className="min">
              {this.formatedPrice(this.props.data.price.min)}
            </span>
          </p>
        </div>
      </li>
    )
  }
}
