import * as React from 'react';
import { Item } from './item';
import axios, { AxiosRequestConfig, AxiosPromise } from 'axios';
import { Service } from './service';
import { Pagination } from './pagination';
import { Loading } from './loading';
import { ItemsPerPage } from './items-per-page';

export class App extends React.Component<any, any>{

  private axios: any;
  private searchInput: any;

  constructor(props: any){
    super(props);

    this.state = {
      products: [],
      searchStr: '',
      page: 0,
      itemsPerPage: 16,
      loading: true
    };

    this.axios = Service.axios;
    this.fetchData();
  }

  fetchData(){
    this.axios.get('vm').then((res: any) => {
      this.setState({
        products: res.data.data,
        loading: false
      })
      // console.log(res.data);
    }).catch((err: any) => {
      console.log(err);
    })
  }

  filter(str: string, array: Array<any>): Array<any>{
    if(str == ''){
      return array;
    }

    let filtered: Array<any> = [];

    for(let item of array){
      if(item.title.match(str)){
        filtered.push(item);
      }
    }

    return filtered;
  }

  search(e: any){
    this.setState({searchStr: e.target.value});
  }

  filteredProducts(): Array<any>{
    return this.filter(this.state.searchStr, this.state.products);
  }

  paginatedProducts(): Array<any>{
    let products = this.filteredProducts();
    let paginated: Array<any> = new Array();
    let start:number = this.state.page * this.state.itemsPerPage;
    let end: number = start + this.state.itemsPerPage;

    for(let i = start; i < end; i++){
      if(products[i]){
        paginated.push(products[i]);
      }
    }

    return paginated;
  }

  pageChangeHandler(page: number){
    this.setState({page: page});
  }

  itemsPerPageChangeHandler(quantity: number){
    this.setState({itemsPerPage: quantity});
  }

  render(){
    return (
      <div className="main">
        <header>
          <h1 className="logo">mmartan</h1>
          <input className="search" type="text" onChange={this.search.bind(this)} />
        </header>
        <ul className="list">
        {this.paginatedProducts().map((product: any, i: number) => {
          return (
              <Item key={i} data={product} />
          )
        })}
        </ul>
        <ItemsPerPage onSelect={this.itemsPerPageChangeHandler.bind(this)} current={this.state.itemsPerPage} />
        <Pagination onPageChange={this.pageChangeHandler.bind(this)} current={this.state.page} pages={(this.state.products.length / this.state.itemsPerPage)} />
        <Loading show={this.state.loading} />
      </div>
    )
  }
}
